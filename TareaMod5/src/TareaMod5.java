public class TareaMod5 
{
    /*
     * En esta clase es donde se encuentra el main
     * que llama las diferentes subclases 
     * y llama los metodos que se encuentran en ella.
       */
    
  public static void main(String arg[]) 
  {
     
    Mugato_BsAs mug= new Mugato_BsAs();//se llama la subclase 1
    mug.mostrarMetodos();
    System.out.println("Modelo = "+mug.modelo()+"\n"+"Tipo = "+mug.tipo()+"\n");
   
     Democrata dem= new  Democrata();//se llama la subclase 2
    dem.mostrarMetodos();
    System.out.println("Modelo = "+dem.modelo()+"\n"+"Tipo = "+dem.tipo()+"\n");
   
    LadyStork lad= new LadyStork();//se llama la subclase 3
    lad.mostrarMetodos();
    System.out.println("Modelo = "+lad.modelo()+"\n"+"Tipo = "+lad.tipo()+"\n");
   
    
   }
 }