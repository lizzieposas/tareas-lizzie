public   class Zapatos 
  {
    ///Variables que de la clase Zapatos
        public String marca;
        public String genero;
        public String  material;
        
    public Zapatos () 
    {
    }
    public Zapatos (String marca, String genero, String material) 
    {
        ///cosntructor , que recibe 3 parametros tipo String , de cada zapato
        this.marca= marca;
        this.genero= genero;
        this.material = material;
    }
    ///metodos de la clase Zapatos
    public void marca(String marca)// metodo#1 es para darle una marca a el zapato
    { 
        marca = marca;
    }
    public void  genero(String genero) //metodo#2 es para darle a que genero va dirigido el zapato
    {
      genero= genero; 
    }
    public void  material(String material) //metodo #3 es para darle el tipo de material del zapato
    {
      material= material; 
    }
    public void mostrarMetodos() //muestra en un mensaje los datos de los zapatos.
    {
        System.out.println("Marca = " + marca+"\n"+"Genero = " + 
        genero+"\n"+"Material = "+material);
    }
   }