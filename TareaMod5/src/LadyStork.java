
/*subclase #3 , resibe los parametros del cosntructor de la
 * clase padre , Zapatos , la marca , el genero y el material .
 * tambien se hacen 2 metodos  tipo String para recibir el modelo
 * y el otro  para recibir el tipo de el szapato.
   */
 public  class LadyStork extends Zapatos 
  {
    public LadyStork() 
    {
        super("Lady Stork","Mujer","Cuero Vacuno");
    }
    public String modelo() {
        String mol= "Paz";
        return mol;
    }
    public String tipo() {
        String tip="Botas";
        return tip;
    }
}
