
/*subclase #2 , resibe los parametros del cosntructor de la
 * clase padre , Zapatos , la marca , el genero y el material .
 * tambien se hacen 2 metodos  tipo String para recibir el modelo
 * y el otro  para recibir el tipo de el szapato.
   */
 public  class Democrata extends Zapatos 
  {
    public Democrata() 
    {
        super("Democrata","Hombre","Cuero Cabra");
    }
    public String modelo() {
        String mol= "111101-008";
        return mol;
    }
    public String tipo() {
        String tip="Borceguíes";
        return tip;
    }
}